var express = require('express');
var app = express();

app.set('port', (process.env.PORT || 8000));
app.use(express.static(__dirname + '/app'));
console.log(__dirname);

app.get('/', function(request, response) {
   response.sendfile('index.html', {root: __dirname})
});

app.listen(app.get('port'), function() {
  console.log("Node app is running at 127.0.0.1:" + app.get('port'));
});

'use strict';

/**
 * @ngdoc function
 * @name toolsApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the toolsApp
 */
angular.module('toolsApp')
  .controller('MainCtrl', ['$scope', function ($scope) {
    $scope.years = [];
    $scope.months = ['January','February','March','April','May','June','July','August','September','October','November','December'];

    $scope.addYear = function() {
        for (var i = 2003; i <= 2030; i++) {
            $scope.years.push(i);
        };
    }

    $scope.date = new Date();
    $scope.addYear();
  }]);

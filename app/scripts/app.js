'use strict';

/**
 * @ngdoc overview
 * @name toolsApp
 * @description
 * # toolsApp
 *
 * Main module of the application.
 */
angular
  .module('toolsApp', []);
